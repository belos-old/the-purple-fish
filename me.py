import pycurl
from StringIO import StringIO
import json
import axon

buffer = StringIO()
c = pycurl.Curl()
c.setopt(c.UNIX_SOCKET_PATH,'/var/lib/lxd/unix.socket')
c.setopt(c.URL, 's/1.0')
__file = open('data.json','w')
c.setopt(c.WRITEDATA,buffer)

#c.setopt(c.UNIX_SOCKET_PATH,'var/lib/lxd/unix.socket')
#__file = open('data.json','w')

c.perform()
c.close()

body = buffer.getvalue()
__file.write(body)

# Body is a string in some encoding.
# In Python 2, we can print it without knowing what the encoding is.#
#print(body)
parsed_json = json.loads(body)


#print "###################################################"
print axon.dumps(parsed_json, pretty=1)


