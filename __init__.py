import ajenti
from ajenti.api import *
from ajenti.plugins import *

info = PluginInfo(
    title = 'LXDPlugin',
    icon = 'folder-close',
    description='LXD Management Tool',
    dependencies=[
        PluginDependency('main'),
    ],
)

def init():
    import main;
